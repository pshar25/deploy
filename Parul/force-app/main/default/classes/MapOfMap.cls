public class MapOfMap {
    public Map<Id,Map<Id,List<Contact>>> mapOf{get;set;}
    Public MapOfMap(){
        mapOf =new  Map<Id,Map<Id,List<Contact>>>();
        
        for (Account acc : [select id, (select id,name from Contacts) from Account]) {
            Map<Id, List<Contact>> innerMap = new Map<Id, List<Contact>>();
            innerMap.put(acc.Id, acc.Contacts);
            mapOf.put(acc.Id, innerMap);
        }
        System.debug(mapOf);
    }
}